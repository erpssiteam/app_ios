# IOS (Swift 3)

IMPORTANT : 

- Le serveur sur lequel est hébergé l'API ne supporte pas les images de plus de 5Mo. L'application ne gérera pas l'envoi de photo plus importante.
    - Pour ajouter une image, vous pouvez drag-and-drop une image directement dans le simulateur, puis l'enregistrer.
    - Les images présentes par défaut sur le simulateur peuvent être envoyées. 
- Si il n'y a pas d'entrée sur la liste, déconnectez-vous et utilisez les identifiants fournis dans ce readme (Gestion token expiré non géré : voir ToDos)


# Equipe
 - Reslène Bahloul
 - Kévin Bazan
 - Stéphane Guillaume
 - Florian Berger

# Résumé
Cette application permet à des utilisateurs de créer et consulter leurs notes de frais pour une entreprise. Elle permet également la visualisation des notes de frais en cours, validées ou refusées. Une image de la facture ou du ticket de caisse est lié à une note de frais et est consultable directement dans l'application.
Pour utiliser l'application, des identifiants de connexion sont requis au préalable. Ces identifiants sont présents dans l'API qui permet l'enregistrement des informations de l'application. Il n'y a pas de modification des notes de frais de disponible et ce comportement est voulu.

# Fonctionnalités

 - Connexion à l'API avec retour JWT token permettant l'identification et l'accès à l'application (stocké en local storage)
 - Gestion de la connexion internet (si pas de connexion, message d'erreur et l'application se ferme)
 - Une fois connecté, affichage du dashboard de l'application avec plusieurs éléments:
   - Bouton permettant d'afficher le formulaire de création d'une note de frais
   - Bouton permettant l'affichage de la liste des notes de frais concernant l'utilisateur connecté
   - Le nombre de notes de frais affilié
   - Des compteurs indiquant le nombre de notes de frais en cours, validées ou refusées. Chaque indicateur permet l'accès à la page liste avec les notes de frais affiliés (uniquement celles en cours, celles validées ou celles refusées)
   - Une bar menu en bas de cette page permettant la déconnexion et l'accès à la liste des notes de frais
 - La liste des notes de frais affichent les descriptions des notes de frais, il est possible de revenir à la page précédentes via le bouton en haut de la page
 - La recherche parmi les résultats est possible grâce au champ de recherche présent en haut de la liste, on peut donc rechercher en écrivant en temps réel, une fois la recherche effectuée, on doit soit cliquer sur la croix pour réinitialiser la recherche ou effacer le champ de recherche (manque de temps pour la recherche reverse quand on efface)
 - Possiblité de supprimer une note de frais en swipant une note de frais sur la gauche et en cliquant le bouton "delete"
 - Accès à la page de détail d'une note de frais où se regroupe toutes les informations (intitulé, description, image, date, montant, tva, et validation le)
- L'image contenu dans le détail d'une note de frais est cliquable et peut être visualisé en fullscreen
- Le formulaire de création d'une note de frais regroupe tous les champs de la page détail. Des validateurs sont présents sur les champs intitulé, description (minimum 5 caractères) ainsi que sur le champ montant (ne peut être vide)
 - Pour le champ montant, le clavier numérique est utilisé avec l'option pour les décimals
- Pour le champ image, un bouton permet de lancer le plugin ImagePicker, qui va permettre la prise d'une photo ou le choix d'une photo de la librairie du téléphone et venir l'afficher dans le formulaire en miniature
- Une fois l'envoi terminé, la liste est de nouveau affiché.
# Installation

Ce projet doit être exécuté sur un MAC via le logiciel Xcode.
Une fois le projet récupéré, vous devez installer les dépendances.
```sh
$ pod install
```
Puis builder le projet pour lancer le simulateur ou à défaut utiliser votre Iphone.

# Connexion à l'application
- Login : user
- Mot de passe : erpssi

# Plugins
| Plugin |
| ------ |
| Alamofire |
| PageMenu |
| ImagePicker |
| Lightbox |
| Validator |
| GSImageViewerController |

# API BACK
L'api back a été développé avec Symfony 2.8 et est actuellement en prod sur un server OVH.

# Todos

- Gestion du token expirée (actuellement le token renvoyé après le succès d'une authentication est de 36000 secondes (devéloppement)), par manque de temps et moyen matériel, nous n'avons pas pu mettre en place le renvoi sur la page de login si le token n'est plus valide.
- Amélioration des contraintes pour un affichage plus adéquat sur les différents supports
- Mise en place du mode paysage (non disponible)

License
----
MIT