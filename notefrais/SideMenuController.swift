//
//  SideMenuController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import SideMenuController

class SideMenuController: SideMenuController {
    
    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        SideMenuController.preferences.drawing.sidePanelWidth = 300
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        super.init(coder: aDecoder)
    }
}
