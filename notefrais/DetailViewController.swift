//
//  DetailViewController.swift
//  notefrais
//
//  Created by stagiaire on 20/03/2017.
//  Copyright © 2017 erpssi. All rights reserved.
//

import UIKit
import GSImageViewerController

class DetailViewController: UIViewController {

    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionDetailLabel: UITextView!
    @IBOutlet weak var dateValidationLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tvaLabel: UILabel!
    
    
    // Formates date to "YYYY-mm-dd"
    func formateDate(date: String, offset: Int) -> String {
        let index = date.index(date.endIndex, offsetBy: offset)
        return date.substring(to: index)
    }
    
    // Affects core data value to labels
    func configureView() {
    
        if let detail = self.detailItem {
            if let descriptionDetail = self.descriptionDetailLabel {
                descriptionDetail.text = (detail.value(forKey: "description")! as! String)
            }
            if let date = self.dateLabel {
                let dateDictionary = detail.value(forKey: "date_expense_account") as! NSDictionary // datetime object is an array
                date.text = self.formateDate(date: (dateDictionary.value(forKey: "date") as! String), offset: -16) // will formate the date
            }
            
            if let tva = self.tvaLabel {
                tva.text = "\(detail.value(forKey: "tva")!) %"
            }
            
            if let dateValidation = self.dateValidationLabel {
                // this date could be <null>
                if detail.value(forKey: "validated_at") is NSNull {
                    dateValidation.text = "-"
                } else {
                    let dateValidationDictionary = detail.value(forKey: "validated_at") as! NSDictionary //datetime object is an array
                    dateValidation.text = self.formateDate(date: dateValidationDictionary.value(forKey: "date") as! String, offset: -16)
                }
            }
            
            if let total = self.totalLabel {
                total.text = "\(detail.value(forKey: "total")!) €"
            }
            
            if let img = self.imageView {
                // this image could be <null>
                let imgUrl = detail.value(forKey: "image")!
                if imgUrl is NSNull {
                    img.image = self.imageView.image
                } else {
                    // the back does not provide http://
                    if let url = NSURL(string: "http://" + "\(imgUrl)"), let data = NSData(contentsOf: url as URL) {
                        img.image = UIImage(data: data as Data)
                    }
               }
            }
            
            if let name = self.nameLabel {
                name.text = (detail.value(forKey: "label")! as! String)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // enable user interaction
        imageView.isUserInteractionEnabled = true
        // add touch action to imageView
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        // Add the recognizer to the view.
        imageView.addGestureRecognizer(tapRecognizer)
        
        self.configureView()
    }
    
    /** Touch, scale and zoom image viewer **/
    func imageTapped(_ gestureRecognizer: UITapGestureRecognizer) {
        // zoom with a modal with GSImageViewerController plugin
        let imageInfo   = GSImageInfo(image: imageView.image!, imageMode: .aspectFill)
        let imageViewer = GSImageViewerController(imageInfo: imageInfo)
        present(imageViewer, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: NSDictionary? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }


}

